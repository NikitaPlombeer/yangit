package com.plombeer.yangit.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by plombeer on 06.07.17.
 */

public class Repository {

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("html_url")
    private String url;

    @SerializedName("language")
    private String language;

    @SerializedName("owner")
    private User user;


    public Repository() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getLanguage() {
        return language;
    }

    public User getUser() {
        return user;
    }
}
