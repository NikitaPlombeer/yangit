package com.plombeer.yangit.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by plombeer on 06.07.17.
 */

public class User {

    @SerializedName("login")
    private String username;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
