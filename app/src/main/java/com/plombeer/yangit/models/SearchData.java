package com.plombeer.yangit.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by plombeer on 06.07.17.
 */

public class SearchData {

    @SerializedName("total_count")
    private int total;

    @SerializedName("items")
    private List<Repository> items;

    public SearchData() {
    }

    public int getTotal() {
        return total;
    }

    public List<Repository> getItems() {
        return items;
    }
}
