package com.plombeer.yangit.dagger.component;

import com.plombeer.yangit.MainActivity;
import com.plombeer.yangit.dagger.module.NetModule;
import com.plombeer.yangit.dagger.module.ServiceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ServiceModule.class, NetModule.class})
public interface NetComponent {
    void inject(MainActivity activity);
}