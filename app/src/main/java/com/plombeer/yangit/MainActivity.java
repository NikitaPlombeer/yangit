package com.plombeer.yangit;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements MaterialSearchView.SearchViewListener, MaterialSearchView.OnQueryTextListener {

    @Inject
    public GithubService githubService;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.searchView)
    public MaterialSearchView searchView;

    @BindView(R.id.rvMain)
    public RecyclerView rvMain;

    @BindView(R.id.progress_wheel)
    public ProgressWheel wheel;

    private RepositoryAdapter repositoryAdapter;

    private String currentSearchText;

    private EndlessRecyclerOnScrollListener endlessScrollListener;

    private Subscription githubSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((App)getApplication()).mNetComponent().inject(this);

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        endlessScrollListener = new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                search(currentSearchText, page);
            }
        };
        repositoryAdapter = new RepositoryAdapter(new ArrayList<>());
        rvMain.setAdapter(repositoryAdapter);
        rvMain.setLayoutManager(mLayoutManager);
        rvMain.addItemDecoration(new DividerItemDecoration(rvMain.getContext(),
                mLayoutManager.getOrientation()));
        rvMain.addOnScrollListener(endlessScrollListener);
        searchView.setOnQueryTextListener(this);

        repositoryAdapter.getPositionClicks().subscribe(repository -> {
            String url = repository.getUrl();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

    }

    public void search(String word, int page) {
        this.currentSearchText = word;
        word = word.replaceAll(" ", "+");
        wheel.setVisibility(View.VISIBLE);
        githubSubscription = githubService.findRepositoriesByKey(word, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchData -> {
                    int total = searchData.getTotal();
                    getSupportActionBar().setTitle("Total: " + total);
                    repositoryAdapter.addData(searchData.getItems());
                    endlessScrollListener.setLoading(false);
                    wheel.setVisibility(View.GONE);
                }, throwable -> {
                    wheel.setVisibility(View.GONE);
                    throwable.printStackTrace();
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.search);
        searchView.setMenuItem(item);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if(githubSubscription != null)
            githubSubscription.unsubscribe();
        endlessScrollListener.reset();
        repositoryAdapter.clear();
        search(query, 1);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onSearchViewShown() {
    }

    @Override
    public void onSearchViewClosed() {}

    @Override
    protected void onStop() {
        super.onStop();
        if(githubSubscription != null)
            githubSubscription.unsubscribe();
        wheel.setVisibility(View.GONE);
    }
}
