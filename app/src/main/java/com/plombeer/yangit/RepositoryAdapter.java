package com.plombeer.yangit;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.plombeer.yangit.models.Repository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by plombeer on 06.07.17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    private static final DisplayImageOptions avatarOptions = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.empty)
            .showImageForEmptyUri(R.drawable.empty)
            .showImageOnFail(R.drawable.empty)
            .displayer(new CircleBitmapDisplayer())
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

    private List<Repository> repositories;
    private final PublishSubject<Repository> onClickSubject = PublishSubject.create();

    public RepositoryAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setOnClickListener(v1 -> {
            int pos = vh.getAdapterPosition();
            if(pos != RecyclerView.NO_POSITION) {
                onClickSubject.onNext(repositories.get(pos));
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Repository repository = repositories.get(position);
        holder.usernameTextView.setText(repository.getUser().getUsername());
        holder.projectNameTextView.setText(repository.getName());
        holder.descriptionTextView.setText(repository.getDescription());
        holder.langTextView.setText(repository.getLanguage());

        ImageLoader.getInstance().displayImage(repository.getUser().getAvatarUrl(), holder.avatarImageView, avatarOptions);
    }

    public Observable<Repository> getPositionClicks(){
        return onClickSubject.asObservable();
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public void addData(List<Repository> items) {
        repositories.addAll(items);
        this.notifyItemRangeInserted(repositories.size() - items.size(), items.size());
    }

    public void clear() {
        repositories.clear();
        this.notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.usernameTextView)
        public TextView usernameTextView;

        @BindView(R.id.avatarImageView)
        public ImageView avatarImageView;

        @BindView(R.id.descriptionTextView)
        public TextView descriptionTextView;

        @BindView(R.id.projectNameTextView)
        public TextView projectNameTextView;

        @BindView(R.id.langTextView)
        public TextView langTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
