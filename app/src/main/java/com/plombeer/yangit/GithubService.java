package com.plombeer.yangit;

import com.plombeer.yangit.models.SearchData;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by plombeer on 06.07.17.
 */

public interface GithubService {

    @GET("search/repositories?")
    Observable<SearchData> findRepositoriesByKey(@Query("q") String key, @Query("page") int page);
}
